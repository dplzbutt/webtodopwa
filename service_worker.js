self.addEventListener('install', function(event) {
    console.log('Service worker installing...');
    });

// Listen for install event, set callback

self.addEventListener('activate', (event) => {
  console.log('Service worker activating...');
});


self.addEventListener('fetch', function(event) {

  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
    );
  // If the requested data isn't in the cache, the response
  // will look like a connection error
});