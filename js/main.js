if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('service_worker.js').then(() => {
        console.log("Service Worker Registered")
    });
}
//build or rebuild the array for local storage or get data
//from the last list created and stored in cache

const input = document.getElementById('inArray');
//var inputAD = document.getElementById('all-done-list');
const inToDoList = document.getElementsByClassName('ui-state-default');
let toDoLocalArray = localStorage.getItem('items') ?
    JSON.parse(localStorage.getItem('items')) : []


//set variables in DOM
localStorage.setItem('items', JSON.stringify(toDoLocalArray))
const dataToDo = JSON.parse(localStorage.getItem('items'))

let allDoneArray = localStorage.getItem('itemsAD') ?
    JSON.parse(localStorage.getItem('itemsAD')) : []

localStorage.setItem('itemsAD', JSON.stringify(allDoneArray))
const dataAllDone = JSON.parse(localStorage.getItem('itemsAD'))



dataToDo.forEach(item => {
    createTodo(item)
})

dataAllDone.forEach(item => {
    done(item)
})
// count initial ToDo
countTodos();

// capture click event
$("#checkAll").click(function () {
    AllDone();
});

//capture enter key press
$('.add-todo').on('keypress', function (e) {
    e.preventDefault // Do not submit form
    if (e.which == 13) { // check if enter is pressed
        //write here
        if (!navigator.geolocation) {
            status.textContent = 'Geolocation is not supported by your browser';
            toDoLocalArray.push(input.value)
            localStorage.setItem('items', JSON.stringify(toDoLocalArray))
            var todo = $(".add-todo").val()
            addToDo(todo);
        } else {
            status.textContent = 'Locating…';
            navigator.geolocation.getCurrentPosition(success, error);
            toDoLocalArray.push(input.value)
            localStorage.setItem('items', JSON.stringify(toDoLocalArray))
            var todo = $(".add-todo").val()
            addToDo(todo);
        }  
    }
});

// capture click event
$('#addTODO').on('click', function () {
    //write here
    toDoLocalArray.push(input.value)
    localStorage.setItem('items', JSON.stringify(toDoLocalArray))
    var todo = $(".add-todo").val();
    addToDo(todo);
});

// capute checkbox value change and transfer from ToDos to Already Done
$('.todolist').on('change', '#sortable li input[type="checkbox"]', function () {
    if ($(this).prop('checked')) {
        //write here
        allDoneArray.push($(this).parent().text())
        localStorage.setItem('itemsAD', JSON.stringify(allDoneArray))
        toDoLocalArray.splice($(this).parent().parent().parent().index(), 1)
        localStorage.setItem('items', JSON.stringify(toDoLocalArray))
        var doneItem = $(this).parent().parent().find('label').text();
        $(this).parent().parent().parent().addClass('remove');
        done(doneItem);
        countTodos();
    }
});

// capture click event on button on Already Done
$('.todolist').on('click', '.remove-item', function () {
    removeItem(this);
});

// add new todo
function addToDo(todo) {
    createTodo(todo);
    countTodos();
}

// count tasks
function countTodos() {
    var count = $("#sortable li").length;
    $('.count-todos').html(count);
}

//create task
function createTodo(text) {
    var markup = '<li class="ui-state-default"><div class="checkbox"><label id="in-to-do-list"><input type="checkbox" value="" />' + text + ' - '+'<span id="status"><a id = "map-link" target="_blank" href=""></a></span></label></div></li>';
    $('#sortable').append(markup);
    $('.add-todo').val('');
}

//mark task as done
function done(doneItem) {
    var done = doneItem;
    var markup = '<li id="all-done-list">' + done + '<button class="btn btn-default btn-xs pull-right  remove-item"><span class="fa fa-minus-square"></span></button></li>';
    $('#done-items').append(markup);
    $('.remove').remove();
}

//mark all tasks as done
function AllDone() {
    var myArray = [];
    //write here
    toDoLocalArray = []
    localStorage.setItem('items', JSON.stringify(toDoLocalArray))
    $('#sortable li').each(function () {
        myArray.push($(this).text());
    });

    // add to done
    for (i = 0; i < myArray.length; i++) {
        $('#done-items').append('<li>' + myArray[i] + '<button class="btn btn-default btn-xs pull-right  remove-item"><span class="glyphicon glyphicon-remove"></span></button></li>');
    }

    // myArray
    $('#sortable li').remove();
    countTodos();
}

//remove done task from list
function removeItem(element) {
    //allDoneArray.splice($(this).parent().index(), 1)
    //localStorage.setItem('itemsAD', JSON.stringify(toDoLocalArray))
    allDoneArray.splice($(element).parent().index(), 1)
    localStorage.setItem('itemsAD', JSON.stringify(allDoneArray))
    $(element).parent().remove();

}

function success(position) {
    const mapLink = document.querySelector('#map-link');
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;

    status.textContent = '';
    mapLink.href = `https://www.openstreetmap.org/#map=18/${latitude}/${longitude}`;
    mapLink.textContent = `Location on Generate ToDo`;
}

function error() {
    status.textContent = 'Unable to retrieve your location';
}



//done-items
//sortable